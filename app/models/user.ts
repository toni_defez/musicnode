import mongoose = require("mongoose");
import { IUser } from "../inteface/user.interface";
var uniqueValidator = require('mongoose-unique-validator');

import { isEmail } from 'validator';

interface IUserModel extends IUser, mongoose.Document { }

var userSchema = new mongoose.Schema({
    createdAt: Date,
    name:{
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    surname:String,
    email:{
        type:String,
        trim: true,
        lowercase: true,
        unique: true,
        validate: [ isEmail, 'invalid email' ]
    },
    password:{
        type:String,
        required:true
    },
    image:String,
    role:{
        type:String,
        default:'ROLE_USER'
    }
});
userSchema.plugin(uniqueValidator);
var User = mongoose.model<IUserModel>("User", userSchema);
export = User;