import mongoose = require("mongoose");
import { ISong } from "../inteface/song.interface";
import { Schema } from "mongoose";


interface ISongModel extends ISong, mongoose.Document { }

var songSchema = new mongoose.Schema({
    number:String,
    name:String,
    duration:String,
    file:String,
    album:{ type: Schema.Types.ObjectId, ref: 'Album' }
});

const Song = mongoose.model<ISongModel>("Song", songSchema);

export default Song;




/***
MODELO JS QUE FUNCIONA.....
'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SongSchema = Schema({
    number:String,
    name:String,
    duration:String,
    file:String,
    album:{
        type:Schema.ObjectId,
        ref:'Album'
    }
});

module.exports = mongoose.model('Song',SongSchema);
 */