import mongoose = require("mongoose");
import {  IAlbum} from "../inteface/album.inteface";
import { Schema } from "mongoose";



interface IAlbumModel extends IAlbum, mongoose.Document { }

var albumSchema = new mongoose.Schema({
    title:{
        type: String,
        trim: true,
        required:true
    },
    description:{
        type: String,
        trim: true,
    },
    image:String,
    year:Number,
    artist:{
        type: Schema.Types.ObjectId, ref: 'Artist',
        required:true
    }
});



const Album = mongoose.model<IAlbumModel>("Album", albumSchema);
export default Album;

/* 
ALBUM ORIGINAL USANDO JS , CON TYPESCRIPT ESTA ARRIBA
'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AlbumSchema = Schema({
    title:String,
    description:String,
    year:Number,
    artist:{
        type:Schema.ObjectId,
        ref:'Artist'
    }
});

module.exports = mongoose.model('Album',AlbumSchema);

**/