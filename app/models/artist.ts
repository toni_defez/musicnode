import mongoose = require("mongoose");
import { IArtist } from "../inteface/artist.interface";
import mongoosePaginate = require('mongoose-paginate');
var uniqueValidator = require('mongoose-unique-validator');
interface IArtistModel extends IArtist, mongoose.Document { }

var artistSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    description:String,
    image:String
});

artistSchema.plugin(mongoosePaginate,uniqueValidator);

var Artist = mongoose.model<IArtistModel>("Artist", artistSchema);
export = Artist;