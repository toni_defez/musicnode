import { Request, Response, NextFunction } from "express";

//añadiendo un campo a la clase Request
declare global{
     namespace Express {
        export interface Request {
           user?: any,
           files?:any
        }
     }
}

import * as jwt from "jwt-simple";
const moment  = require("moment");
const secret = "MusicLiveDaw";

export const EnsureAuth = (req:Request,res:Response,next:any)=>{

    let authorization:string = req.headers.authorization?req.headers.authorization:"";
    let payload ;
    if(!authorization || authorization==""){
        return res.status(400).send({message:'Peticion sin cabecera authorization'});
    }
    else{
        let token = authorization.replace(/['"]+/g,'');
        try {
             payload = jwt.decode(token,secret);

            if(payload.exp <=moment().unix()){
                res.status(404).send({message:'Token ha expirado'});
            }

        } catch (error) {
            res.status(404).send({message:'Token no valido'});
        }
    }
    req.user = payload;
    next();

    
}