import { Request, Response, NextFunction } from "express";
import * as jwt from "jwt-simple";
const moment  = require("moment");
const secret = "MusicLiveDaw";

export const createToken = (user:any)=>{

    let payload = {
        sub: user._id,
        name: user.name,
        surname: user.surname,
        role:user.role,
        image:user.image,
        iat:moment().unix(),
        exp:moment().add(30,'days').unix
    };

    return jwt.encode(payload,secret);
};

