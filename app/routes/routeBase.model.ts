import { Router } from "express";

export abstract class RouterBase{

    protected path:string;
    public router: Router;

    constructor(path:string){
        this.path = path;
        this.router =  Router();
    }

    initializeRoutes() {
    }



}