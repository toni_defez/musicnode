import { UserController } from '../controllers/user.controller';
import { RouterBase } from './routeBase.model';
import * as authentication from '../middlewares/authenticated';
const multipart = require('connect-multiparty');
const m_upload = multipart({uploadDir:'./uploads/users'});



export class UserRouter extends RouterBase{

    private controller:UserController;
    
    constructor(){
        super('/user');
        this.controller =new UserController();
        this.initializeRoutes();
    }

    initializeRoutes() {
        
        this.router.get(`${this.path}/getAll`, this.controller.getAllUser);
   
        this.router.put(`${this.path}/update-user/:id`,authentication.EnsureAuth,this.controller.updateUser);
        this.router.post(`${this.path}/upload-image-user/:id`,
                    [authentication.EnsureAuth,m_upload]
                    ,this.controller.uploadImage);
        this.router.get(`${this.path}/get-image-user/:imageFile`,this.controller.getImageFile);
       
       
    }
}

