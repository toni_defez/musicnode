import { Router} from 'express';
import { RouterBase } from './routeBase.model';
import { WelcomeController } from '../controllers/welcome.controller';

import * as authentication from '../middlewares/authenticated';


export class WelcomeRouter extends RouterBase{
  
    private controller:WelcomeController;
    
    constructor(){
        super('/welcome');
        this.controller = new WelcomeController();
        this.initializeRoutes();
    }
    initializeRoutes() {
        this.router.get(`${this.path}/saluda`,authentication.EnsureAuth, this.controller.greeting);
       // console.log(this.router.stack);
    }
}

