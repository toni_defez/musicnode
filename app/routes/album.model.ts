import { Router} from 'express';
import { RouterBase } from './routeBase.model';
import * as authentication from '../middlewares/authenticated';
import { AlbumController } from '../controllers/album.controller';
import Album from '../models/album';


const multipart = require('connect-multiparty');
const m_upload = multipart({uploadDir:'./uploads/song'});

export class AlbumRouter extends RouterBase{

    private controller:AlbumController;

    constructor(){
        super('/album');
        this.controller = new AlbumController();
        this.initializeRoutes();
    }
    initializeRoutes(): void {
        //Sin seguridad por ahora
        this.router.get(`${this.path}/getAll`, this.controller.getAll);
        this.router.post(`${this.path}/save`, this.controller.save);
        this.router.get(`${this.path}/artist/:artist?`,this.controller.getAlbumByArtist);
        this.router.get(`${this.path}/:id`, this.controller.getAlbumId);
        this.router.put(`${this.path}/:id`, this.controller.updateAlbum);
        this.router.delete(`${this.path}/:id`,this.controller.deleteAlbum);

        this.router.post(`${this.path}/upload-image-album/:id`,
        [authentication.EnsureAuth,m_upload]
        ,this.controller.uploadImage);

        this.router.get(`${this.path}/get-image-album/:imageFile`,this.controller.getImageFile)
     
    }


}