import { RouterBase } from "./routeBase.model";
import { SongController } from "../controllers/song.controller";
const multipart = require('connect-multiparty');
const m_upload = multipart({uploadDir:'./uploads/song'});
import * as authentication from '../middlewares/authenticated';

export class SongRouter extends RouterBase{
    private controller:SongController;

    constructor(){
        super('/song');
        this.controller =new SongController();
        this.initializeRoutes();
    }

    initializeRoutes() {
        this.router.get(`${this.path}/getAll`, this.controller.getAllSong);
        this.router.post(`${this.path}/save`,this.controller.saveSong);
        this.router.get(`${this.path}/album/:album?`,this.controller.getSongsByAlbum);
        this.router.get(`${this.path}/:id`,this.controller.getSongId);
        this.router.put(`${this.path}/:id`,this.controller.updateSong);
        this.router.delete(`${this.path}/:id`,this.controller.deleteSong);

        this.router.post(`${this.path}/upload-file-song/:id`, [m_upload],this.controller.uploadFile);
        this.router.get(`${this.path}/get-file-song/:songFile`,this.controller.getSongFile);

    }

}