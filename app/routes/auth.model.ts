import { RouterBase } from "./routeBase.model";
import { AuthController } from "../controllers/auth.controller";

export class AuthRouter extends RouterBase{
    
    private controller:AuthController;
    
    constructor(){
        super('/auth');
        this.controller =new AuthController();
        this.initializeRoutes();
    }

    initializeRoutes() {
        this.router.post(`${this.path}/register`,this.controller.registerUser);
        this.router.post(`${this.path}/login`,this.controller.loginUser);
        this.router.get(`${this.path}/token`,this.controller.validateToken);
    }
}