import { Router} from 'express';
import { RouterBase } from './routeBase.model';
import { ArtistController } from '../controllers/artist.controller';
import * as authentication from '../middlewares/authenticated';


const multipart = require('connect-multiparty');
const m_upload = multipart({uploadDir:'./uploads/artist'});

export class ArtistRouter extends RouterBase{
    
    private controller:ArtistController;

    constructor(){
        super('/artist');
        this.controller = new ArtistController();
        this.initializeRoutes();
    }

    initializeRoutes(): void {
        //AÑADIR MIDLEWARE DE AUTHENTICATION
        this.router.get(`${this.path}/getAll`, this.controller.getAllArtist);
        this.router.post(`${this.path}/save-artist`, this.controller.saveArtist);
        this.router.get(`${this.path}/detail/:id`,this.controller.getArtistID);
        this.router.get(`${this.path}/:id`,this.controller.getArtists);
        this.router.put(`${this.path}/:id`,this.controller.updateArtist);
        this.router.delete(`${this.path}/:id`,this.controller.deleteArtist);

        this.router.post(`${this.path}/upload-image-artist/:id`,
        [m_upload]
        ,this.controller.uploadImage);

        this.router.get(`${this.path}/get-image-artist/:imageFile`,this.controller.getImageFile)
  
    }

}