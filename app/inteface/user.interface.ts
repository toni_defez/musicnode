export interface IUser{
    name:String,
    surname:String,
    email:String,
    password:String,
    image:String,
    role:String
}
 