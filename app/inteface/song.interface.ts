import { IAlbum } from "./album.inteface";

export interface ISong{
    number:String,
    name:String,
    duration:String,
    file:String,
    album:IAlbum
}