import { IArtist } from "./artist.interface";

export interface IAlbum{
    title:String,
    description:String,
    year:Number,
    artist:IArtist,
    image:String
}