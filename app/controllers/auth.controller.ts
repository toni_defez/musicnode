import User = require("../models/user");
import { IUser } from '../inteface/user.interface';
import { Request, Response } from 'express';
import * as jwt2 from "jwt-simple";
import * as bcrypt from 'bcrypt-nodejs';
import * as jwt from '../middlewares/checkJWT';
const secret = "MusicLiveDaw";
const moment = require("moment");

export class AuthController {

    /**
     * Metodo para logear a un usuario
     * @param req 
     * @param res 
     */
    loginUser(req: Request, res: Response) {
        let params = req.body;
        let email = params.email;
        let password = params.password;


        let user = User.findOne({ "email": email }, (err: any, user: any) => {
            if (err) {
                res.status(500).send({ message: "Error en la peticion", error: err });
            } else {
                if (!user) {
                    res.status(404).send({ message: "El usuario no existe" });
                }else {
                    bcrypt.compare(password, user.password + "", (error: any, isMatch: boolean) => {
                        if (isMatch) {
                            if (params.gethash) {
                                res.status(200).send({
                                    token: jwt.createToken(user)
                                })
                            }else {
                                res.status(200).send({ user });
                            }
                        }else {
                            res.status(404).send({ message: "Contraseña incorrecta" });
                        }
                    });
                }
            }
        });
    }

    /**
     * Metodo para registrar usuario 
     * @param req 
     * @param res 
     */
    registerUser(req: Request, res: Response) {

        let checkPassword = function doSthWithUser(user: IUser) {

            if (user.password) {
                bcrypt.hash(user.password.toString(), '', () => { }, (err: any, hash: any) => {
                    user.password = hash;
                });
                return true;
            }
            else {
                return false;
            }

        }
        var user = new User(req.body);
        if (checkPassword(user)) {
            user.save((error, userStored) => {
                if (error) {
                    res.status(500).send({ message:"Error al registrar usuario",error:error });
                } else {
                    if (!userStored) {
                        res.status(404).send({ message: 'No se ha registrado el usuario' });
                    }
                    else {
                        res.status(200).send({
                            "ok": true,
                            "user": userStored
                        });
                    }
                }
            });

        }
        else {
            res.status(200).send({ message: 'Introduzca contraseña' });
        }
    }

    /**
     * Metodo para validar token
     * @param req 
     * @param res 
     */
    validateToken(req: Request, res: Response) {

        let authorization: string = req.headers.authorization ? req.headers.authorization : "";
        let payload;
        let resultado = {};
        if (!authorization || authorization == "") {
            resultado = {
                error: false
            };
        }
        else {
            let token = authorization.replace(/['"]+/g, '');
            try {
                payload = jwt2.decode(token, secret);

                if (payload.exp <= moment().unix()) {
                    resultado = {
                        error: false
                    };
                }
                else {
                    resultado = {
                        error: true
                    };
                }

            } catch (error) {
                resultado = {
                    error: false
                };
            }
        }
        res.send(JSON.stringify(resultado));
    }
}