import { Request, Response } from 'express';
import * as fs from 'fs';
import * as path from 'path';

import User = require("../models/user");

export class UserController {

    /**
     * Obtenemos todos los usuarios registrados en la aplicacion
     * @param req 
     * @param res 
     */
    getAllUser(req: Request, res: Response) {
        let users = User.find((err: any, users: any) => {
            if (err) {
                res.status(500).send({
                    message: 'Error'
                });
            } else {
                res.status(200).send({
                    users: users
                });
            }
        });
    };


    /**
     * Metodo para actualizar usuario
     * @param req 
     * @param res 
     */
    updateUser(req: Request, res: Response) {
        let userId = req.params.id;
        let update = req.body;

        User.findByIdAndUpdate(userId, update, async (err, userUpdated) => {
            if (err) {
                res.status(500).send({ message: 'Error al actualizar usuario' });
            }
            else {
                if (!userUpdated) {
                    res.status(404).send({ message: 'No se ha podido actualizar al usuario' });
                }
                else {
                    let current_User = await User.findById(userUpdated._id);
                    res.status(200).send(
                        {
                            ok: true,
                            user: current_User
                        }
                    );
                }
            }
        });
    }

    /**
     * Metodo para subir imagen de usuario de perfil
     * @param req 
     * @param res 
     */
    uploadImage(req: Request, res: Response) {
        let userId = req.params.id + "";
        let file_name: string = 'No subido....';

        if (req.files) {
            let image: any = req.files.image;
            let file_split: Array<string> = image.path.split('/');
            file_name = file_split[2];

            if (image.type == 'image/jpeg' || image.type == 'image/png' || image.type == 'image/gif') {
                User.findByIdAndUpdate(userId, { image: file_name }, (err, userUpdate) => {
                    console.log(err);
                    if (err) {
                        res.status(500).send({ message: 'Error al actualizar usuario' });
                    }
                    else {
                        if (!userUpdate) {
                            res.status(404).send({ message: 'No se ha podido actualizar al usuario' });
                        }
                        else {

                            let current_User = User.findById(userUpdate._id);
                            res.status(200).send({ ok: true, user: current_User });
                        }
                    }
                });

            }
            else {
                res.status(200).send({ message: 'Extension del archivo no valida' });
            }
        }
        else {
            res.status(200).send({ message: 'No has subido ninguna imagen...' })
        }
    }

    /**
     * Metodo para obtener la imagen del perfil del usuario 
     * @param req 
     * @param res 
     */
    getImageFile(req: Request, res: Response) {
        let imageFile: string = req.params.imageFile;
        let path_File: string = './uploads/users/' + imageFile;

        fs.exists(path_File, (exists: boolean) => {
            if (exists) {
                res.status(200).sendFile(path.resolve(path_File));
            } else {
                res.status(200).send({ message: "No existe la imagen" });
            }
        });
    }
}


