import {Request, Response } from 'express';

export class WelcomeController{
    
    greeting(req: Request, res: Response) {
        res.send('Hola, World!');
    };

    greeting2(req: Request, res: Response) {
        let { name } = req.params;
        res.send(`Hello, ${name}`);
    }
}