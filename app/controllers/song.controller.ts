import { Request, Response } from 'express';
import Song from "../models/song";
import Album from '../models/album';
import * as fs from 'fs';
import * as path from 'path';


export class SongController {

    /**
     * Obtenemos todas las canciones de la base de datos
     * @param req 
     * @param res 
     */
    getAllSong(req: Request, res: Response) {
        let songs = Song.find((err: any, songs: any) => {
            if (err) {
              res.status(500).send({
                  message:'Error'
              });
            } else {
              res.status(200).send({
                  ok:true,
                  songs:songs
              });
            }
          });
    };

    /**
     * Guardamos la cancion que es pasada por Post
     * @param req 
     * @param res 
     */
    saveSong (req:Request,res:Response){
        let song = new Song(req.body);
        
        song.save((error,songStored)=>{
            if(error){
                res.status(500).send({message:'Error al guardar la cancion',error:error});
            }else
            {
                if(!songStored){
                    res.status(404).send({message:'No se ha  guardado la cancion'});
                }
                else{
                    res.status(200).send({
                        "ok":true,
                        "song":songStored
                    });
                }
            }
        });
    }

    /**
     * Obtenemos la cancion por ID
     * @param req 
     * @param res 
     */
    getSongId(req:Request,res:Response){
        let songId = req.params.id;

        Song.findById(songId).populate({path:'album'}).exec((error,songStored)=>{
            if(error){
                res.status(500).send({message:'Error en la peticion',error:error});
            }
            else{
                if(!songStored){
                    res.status(404).send({message:'Album no encontrado'});
                }else{
                    res.status(200).send({ok:true,song:songStored});
                }
            }
        });

    }

    /**
     * Sacamos todas las canciones por album, basandonos en el parametros Album
     * que nos pasan , en caso de no pasarnos ninguno sacaremos todos los albums
     * @param req 
     * @param res 
     */
    getSongsByAlbum(req:Request,res:Response){
        let albumId= req.params.album;
        let find:any;
        if(!albumId){
            find = Song.find({}).sort('number');
        }
        else {
            find = Song.find({album:albumId}).sort('number');
        }

        find.populate({
            path:'album',
            populate:{
                path:'artist',
                model:'Artist'
            }
        }).exec((error:any,songs:any)=>{
            if(error){
                res.status(500).send({message:'Error en la peticion',error:error});
            }
            else{
                if(!songs){
                    res.status(404).send({message:'No hay canciones'});
                }else{
                    res.status(200).send({ok:true,sons:songs});
                }
            }
        })


    }

    /**
     * Obtenemos el fichero de audio asociado a la cancion
     * @param req 
     * @param res 
     */
    updateSong(req:Request,res:Response){
        let songId = req.params.id;
        let update = req.body;

        Song.findByIdAndUpdate(songId,update,async (err,songUpdate)=>{
            if(err){
                res.status(500).send({message:'Error al actualizar cancion'});
            }
            else{
                if(!songUpdate){
                    res.status(404).send({message:'No se ha podido actualizar la cancion'});
                }
                else{
                    let currentSong = await Song.findById(songUpdate._id);
                    res.status(200).send({ok:true,song:currentSong});
                }
            }
        });
    }

    /**
     * Borramos la cancion asignada
     * @param req 
     * @param res 
     */
    deleteSong(req:Request,res:Response){
        let songId = req.params.id;
        Song.findByIdAndRemove(songId,(error:any,songRemoved:any)=>{
            if(error){
                res.status(500).send({message:'Error en el servidor',error:error});
            }
            if(!songRemoved){
                res.status(404).send({message:'No se ha podido borrar las cancion'});
            }else{
                res.status(200).send({ok:true,song:songRemoved});
            }
        });
    }

    /**
     * Metodo para subir fichero de audio 
     * @param req 
     * @param res 
     */
    uploadFile(req:Request,res:Response){
        let songId = req.params.id+"";
        let file_name:string = 'No subido....';
        console.log(req.files);
        
        if(req.files){
            let image:any = req.files.file;
            let file_split:Array<string> = image.path.split('/');
            file_name = file_split[2];
           
            if(image.type =='audio/mpeg'){
                Song.findByIdAndUpdate(songId,{file:file_name},(err,songUpdate)=>{
                    if(err){
                        res.status(500).send({message:'Error al actualizar cancion',error:err});
                    }
                    else{
                        if(!songUpdate){
                            res.status(404).send({message:'No se ha podido actualizar al artist'});
                        }
                        else{
                            let currentSong = Song.findById(songUpdate._id);
                            res.status(200).send({ok:true,song:currentSong});
                        }
                    }
                });
            }
            else{
                res.status(200).send({message:'Extension del archivo no valida'});
            }
        }
        else{
            res.status(200).send({message:'No has subido ninguna imagen...'})
        }
    }

    /**
     * Obtenemos el fichero de audio asociado
     * @param req 
     * @param res 
     */
    getSongFile(req:Request,res:Response){
        let imageFile:string = req.params.songFile;
        let path_File:string = './uploads/song/'+imageFile;

        fs.exists(path_File,(exists:boolean)=>{
            if(exists){
                res.status(200).sendFile(path.resolve(path_File));
            }else{
                res.status(200).send({message:"No existe la imagen"});
            }
        });
    }
 }