import { Request, Response } from 'express';
import * as fs from 'fs';
import * as path from 'path';

import Artist = require("../models/artist");
import { IArtist } from '../inteface/artist.interface';
import Song from "../models/song";
import { PaginateOptions, PaginateResult } from 'mongoose';
import Album from '../models/album';


export class ArtistController {

    /**
     * Obtengo a todos los artistas
     * @param req 
     * @param res 
     */
    getAllArtist(req: Request, res: Response) {

        Artist.find((err: any, artists: any) => {
            if (err) {
                res.status(500).send({ message: "Error!" });
            } else {
                res.send({
                    ok: true,
                    artists: artists
                });
            }
        });
    };

    /**
     * Guardo a un nuevo artista
     * @param req 
     * @param res 
     */
    saveArtist(req: Request, res: Response) {
        let artist = new Artist(req.body);

        artist.save((error, artistStored) => {
            if (error) {
                res.status(500).send({ message: 'Error al guardar el artista',error:error });
            } else {
                if (!artistStored) {
                    res.status(404).send({ message: 'No se ha registrado el artista' });
                }
                else {
                    res.status(200).send({
                        "ok": true,
                        "artist": artistStored
                    });
                }
            }
        });
    }

    /**
     * Obtengo un artista por Id
     * @param req 
     * @param res 
     */
    getArtistID(req: Request, res: Response) {

        let artistId = req.params.id;
        Artist.findById(artistId, (err, artist) => {
            if (err) {
                res.status(500).send({ message: 'Error en la peticion' });
            } else {
                if (!artist) {
                    res.status(404).send({ message: 'El artista no existe' });
                }
                else {
                    res.status(200).send({
                        ok: true,
                        artist: artist
                    });
                }
            }

        });

    }

    /**
     * Obtengo los artistas paginados
     * @param req 
     * @param res 
     */
    getArtists(req: Request, res: Response) {
        let artistId = req.params.id;
        console.log("Hola", artistId);
        let itemPerPage = 3;
        let options: PaginateOptions = {} as PaginateOptions;
        options.sort = { 'name': 1 };
        options.page = artistId;
        options.limit = 5;

        Artist.paginate({}, options, (err: any, value: PaginateResult<IArtist>) => {
            if (err) {
                console.log(err);
                return res.status(500).send(err);
            }
            console.log('total: ' + value.total);
            console.log('limit: ' + value.limit);
            console.log('page: ' + value.page);
            console.log('pages: ' + value.pages);
            console.log('offset: ' + value.offset);
            console.log('docs: ');
            console.dir(value.docs);
            return res.status(200).send(
                {
                    albums: value.docs,
                    page: value.page,
                }
            )

        })
    }

    /**
     * Modifico y devuelvo artista con los ultimos valores
     * @param req 
     * @param res 
     */
    updateArtist(req: Request, res: Response) {
        let artistId = req.params.id;
        let update = req.body;

        Artist.findByIdAndUpdate(artistId, update, async (err, artistUpdate) => {
            if (err) {
                res.status(500).send({ message: 'Error al actualizar artista' });
            }
            else {
                if (!artistUpdate) {
                    res.status(404).send({ message: 'No se ha podido actualizar al artista' });
                }
                else {
                    let currentArtist = await Artist.findById(artistUpdate._id);
                    res.status(200).send({ ok: true, artist: currentArtist });
                }
            }
        });
    }

    /**
     * Borrar artista por Id
     * @param req 
     * @param res 
     */
    deleteArtist(req: Request, res: Response) {
        let artistId = req.params.id;

        Artist.findByIdAndRemove(artistId, (err, artistRemoved) => {
            if (err) {
                res.status(500).send({ message: 'Error al borrar artista' });
            }
            else {
                if (!artistRemoved) {
                    res.status(404).send({ message: 'No se ha podido borrar al artista' });
                }
                else {
                    //Borramos album
                    Album.find({ artist: artistRemoved._id }).remove((error: any, albumRemoved: any) => {
                        if (!albumRemoved || error) {
                            res.status(404).send({ message: 'No se ha podido borrar al album' });
                        } else {
                            Song.find({ artist: artistRemoved._id }).remove((error: any, songRemoved: any) => {
                                if (!songRemoved) {
                                    res.status(404).send({ message: 'No se ha podido borrar las canciones' });
                                } else {
                                    res.status(200).send({ message: 'Todo borrado' });
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    /**
     * Subir imagen del artista
     * @param req 
     * @param res 
     */
    uploadImage(req: Request, res: Response) {
        let artistId = req.params.id;
        let userId = req.params.id + "";
        let file_name: string = 'No subido....';


        if (req.files) {
            let image: any = req.files.image;
            let file_split: Array<string> = image.path.split('/');
            file_name = file_split[2];

            if (image.type == 'image/jpeg' || image.type == 'image/png' || image.type == 'image/gif') {
                Artist.findByIdAndUpdate(userId, { image: file_name }, async (err, artistUpdate) => {
                    console.log(err);
                    if (err) {
                        res.status(500).send({ message: 'Error al actualizar artist' });
                    }
                    else {
                        if (!artistUpdate) {
                            res.status(404).send({ message: 'No se ha podido actualizar al artist' });
                        }
                        else {
                            let currentArtist = await Artist.findById(artistUpdate._id);
                            res.status(200).send({ ok: true, artist: currentArtist });
                        }
                    }
                });

            }
            else {
                res.status(200).send({ message: 'Extension del archivo no valida' });
            }
        }
        else {
            res.status(200).send({ message: 'No has subido ninguna imagen...' })
        }
    }

    getImageFile(req: Request, res: Response) {
        let imageFile: string = req.params.imageFile;
        let path_File: string = './uploads/artist/' + imageFile;

        fs.exists(path_File, (exists: boolean) => {
            if (exists) {
                res.status(200).sendFile(path.resolve(path_File));
            } else {
                res.status(200).send({ message: "No existe la imagen" });
            }
        });
    }

}