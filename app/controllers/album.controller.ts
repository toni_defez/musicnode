import Album from "../models/album";
import { Request, Response } from 'express';
import Song from "../models/song";
import * as fs from 'fs';
import * as path from 'path';
import { isMongoId } from 'validator';

export class AlbumController { 

    /**
     * Obtenemos todos los albums
     * @param req 
     * @param res 
     */
    getAll(req: Request, res: Response) {
       
        let albums = Album.find((err: any, albums: any) => {
            if (err) {
              res.status(500).send({message:'Error'});
            } else {
              res.status(200).send({albums:albums});
            }
          });
    };

    /**
     * Guardamos un nuevo album
     * @param req 
     * @param res 
     */
    save (req:Request,res:Response){
        let album = new Album(req.body);

        album.save((error,albumStored)=>{
            if(error){
                res.status(500).send({message:'Error al guardar el album',error:error});
            }else{
                if(!albumStored){
                    res.status(404).send({message:'No se ha registrado el album'});
                }
                else{
                    res.status(200).send({
                        "ok":true,
                        "album":albumStored,
                    });
                }
            }
        });
    }

    /**
     * Obtenemos un album por id
     * @param req 
     * @param res 
     */
    getAlbumId(req:Request,res:Response){
        let albumId = req.params.id;

        if(isMongoId(albumId)){
            Album.findById(albumId).populate({path:'artist'}).exec((error,albumStored)=>{
                if(error){
                    res.status(500).send({message:'Error en la peticion',error:error});
                }else{
                    if(!albumStored){
                        res.status(404).send({message:'Album no encontrado'});
                    }else{
                        res.status(200).send({albumStored});
                    }
                }
            });
        }
        else{
            res.status(404).send({message:'valor no encontrado'});
        }
       

    }

    /**
     * Obtenemos todos los albums por artista
     * @param req 
     * @param res 
     */
    getAlbumByArtist(req:Request,res:Response){
        let artistId = req.params.artist? req.params.artist: "";
        let find:any;
        if(!artistId && artistId == ""){
            find = Album.find({}).sort('title');
        }else{
            find = Album.find({artist:artistId}).sort('year');
        }
        find.populate({path:'artist'}).exec((error:any,albums:any)=>{
            if(error){
                res.status(500).send({message:'Error en la peticion',error:error});
            }else{
                if(!albums){
                    res.status(404).send({message:'Artista sin albums'});
                }else{
                    res.status(200).send({ok:true,albums:albums});
                }
            }

        })
    }

    /**
     * Actualizamos un album 
     * @param req 
     * @param res 
     */
    updateAlbum(req:Request,res:Response){
        let albumId = req.params.id;
        let update = req.body;

        Album.findByIdAndUpdate(albumId,update,async (err,albumUpdate)=>{
            if(err){
                res.status(500).send({message:'Error al actualizar album'});
            }
            else{
                if(!albumUpdate){
                    res.status(404).send({message:'No se ha podido actualizar al album'});
                }
                else{
                    let currentAlbum = await Album.findById(albumId._id);
                    res.status(200).send({ok:true,album:currentAlbum});
                }
            }
        });
    }

    /**
     * Borramos un album por id 
     * @param req 
     * @param res 
     */
    deleteAlbum(req:Request,res:Response){
        let albumId = req.params.id;

        Album.findByIdAndRemove(albumId,(error:any,albumRemoved:any)=>{
            if(error){
                res.status(500).send({message:'Error al borrar album',error:error})
            }
            if(!albumRemoved ){
                res.status(404).send({message:'No se ha podido borrar al album'});
            }
            else{
                Song.find({album:albumRemoved._id}).remove((error:any,songRemoved:any)=>{
                    if(!songRemoved){
                        res.status(404).send({message:'No se ha podido borrar las canciones'});
                    }else{
                        res.status(200).send({ok:true,album:albumRemoved});
                    }
                });
            }
            
        });
    }

    
    /**
     * Actualizamos la imagen del album 
     * @param req 
     * @param res 
     */
    uploadImage(req:Request,res:Response){
       
        let albumId = req.params.id+"";
        let file_name:string = 'No subido....';

        if(req.files){
            let image:any = req.files.image;
            let file_split:Array<string> = image.path.split('/');
            file_name = file_split[2];
            
            if(image.type =='image/jpeg' || image.type =='image/png' || image.type == 'image/gif'){
                Album.findByIdAndUpdate(albumId,{image:file_name},async (err,albumUpdate)=>{
                    if(err){
                        res.status(500).send({message:'Error al actualizar artist',error:err});
                    }
                    else{
                        if(!albumUpdate){
                            res.status(404).send({message:'No se ha podido actualizar al artist'});
                        }
                        else{
                            let currentAlbum = await Album.findById(albumUpdate._id);
                            res.status(200).send({ok:true,album:currentAlbum});
                        }
                    }
                });

            }
            else{
                res.status(200).send({message:'Extension del archivo no valida'});
            }
        }
        else{
            res.status(200).send({message:'No has subido ninguna imagen...'})
        }
    }

    /**
     * Obtenemos la imagen de un album
     * @param req 
     * @param res 
     */
    getImageFile(req:Request,res:Response){
        let imageFile:string = req.params.imageFile;
        let path_File:string = './uploads/album/'+imageFile;

        fs.exists(path_File,(exists:boolean)=>{
            if(exists){
                res.status(200).sendFile(path.resolve(path_File));
            }else{
                res.status(200).send({message:"No existe la imagen"});
            }
        });
    }


}