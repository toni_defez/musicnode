import { App } from './app';
import { UserRouter } from './routes/user.model';
import {WelcomeRouter} from './routes/welcome.model';
import { ArtistRouter } from './routes/artist.model';
import { AlbumRouter } from './routes/album.model';
import { SongRouter } from './routes/song.model';
import { AuthRouter } from './routes/auth.model';

const app = new App(
  [
    new UserRouter(),
    new WelcomeRouter(),
    new ArtistRouter(),
    new AlbumRouter(),
    new SongRouter(),
    new AuthRouter()
  ]);
   
app.listen();