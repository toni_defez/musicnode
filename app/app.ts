
import express = require('express');
const mongoose = require('mongoose');
import { WelcomeRouter,UserRouter} from '.';
import * as bodyParser from "body-parser";
import { RouterBase } from './routes/routeBase.model';

export class App {
    public app: express.Application;
    public port: any = process.env.PORT || 3000;

    constructor(routers: RouterBase[]) {
        this.app = express();
        this.connectToTheDatabase();
        this.initializeMiddlewares();
        this.initializeRouters(routers);
      }

      private connectToTheDatabase() {
        mongoose.Promise = global.Promise;
        mongoose.connect('mongodb://localhost:27017/musicLive_db',{ useNewUrlParser: true });
      }

      private initializeMiddlewares() {
        this.app.use(bodyParser.urlencoded({'extended':true})); // parse application/x-www-form-urlencoded
        this.app.use(bodyParser.json()); // parse application/json

        this.app.use((req,res,next)=>{
          res.header('Access-Control-Allow-Origin','*');
          res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY,'+
           'Origin, X-Requested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
          res.header('Access-Control-Allow-Methods','GET,POST,OPTIONS,PUT,DELETE');
          res.header('Allow','GET,POST,OPTIONS,PUT,DELETE');
          next();
        })
      }

      private initializeRouters(controllers: RouterBase[]) {
     
        controllers.forEach((router) => {
          this.app.use('/api', router.router);
        });
      }

      public listen() {
        
        this.app.listen(this.port, () => {
            console.log(`Listening at http://localhost:${this.port}/`);
        });
       
      }
}