"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var mongoose = require('mongoose');
var bodyParser = __importStar(require("body-parser"));
var App = /** @class */ (function () {
    function App(routers) {
        this.port = process.env.PORT || 3000;
        this.app = express();
        this.connectToTheDatabase();
        this.initializeMiddlewares();
        this.initializeRouters(routers);
    }
    App.prototype.connectToTheDatabase = function () {
        mongoose.Promise = global.Promise;
        mongoose.connect('mongodb://localhost:27017/musicLive_db', { useNewUrlParser: true });
    };
    App.prototype.initializeMiddlewares = function () {
        this.app.use(bodyParser.urlencoded({ 'extended': true })); // parse application/x-www-form-urlencoded
        this.app.use(bodyParser.json()); // parse application/json
        this.app.use(function (req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY,' +
                'Origin, X-Requested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
            res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,DELETE');
            res.header('Allow', 'GET,POST,OPTIONS,PUT,DELETE');
            next();
        });
    };
    App.prototype.initializeRouters = function (controllers) {
        var _this = this;
        controllers.forEach(function (router) {
            _this.app.use('/api', router.router);
        });
    };
    App.prototype.listen = function () {
        var _this = this;
        this.app.listen(this.port, function () {
            console.log("Listening at http://localhost:" + _this.port + "/");
        });
    };
    return App;
}());
exports.App = App;
