"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var jwt = __importStar(require("jwt-simple"));
var moment = require("moment");
var secret = "MusicLiveDaw";
exports.EnsureAuth = function (req, res, next) {
    var authorization = req.headers.authorization ? req.headers.authorization : "";
    var payload;
    if (!authorization || authorization == "") {
        return res.status(400).send({ message: 'Peticion sin cabecera authorization' });
    }
    else {
        var token = authorization.replace(/['"]+/g, '');
        try {
            payload = jwt.decode(token, secret);
            if (payload.exp <= moment().unix()) {
                res.status(404).send({ message: 'Token ha expirado' });
            }
        }
        catch (error) {
            res.status(404).send({ message: 'Token no valido' });
        }
    }
    req.user = payload;
    next();
};
