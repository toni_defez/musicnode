"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
//exportamos los routers
__export(require("./routes/welcome.model"));
__export(require("./routes/user.model"));
