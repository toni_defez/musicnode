"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var mongoose_1 = require("mongoose");
var songSchema = new mongoose.Schema({
    number: String,
    name: String,
    duration: String,
    file: String,
    album: { type: mongoose_1.Schema.Types.ObjectId, ref: 'Album' }
});
var Song = mongoose.model("Song", songSchema);
exports.default = Song;
/***
MODELO JS QUE FUNCIONA.....
'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SongSchema = Schema({
    number:String,
    name:String,
    duration:String,
    file:String,
    album:{
        type:Schema.ObjectId,
        ref:'Album'
    }
});

module.exports = mongoose.model('Song',SongSchema);
 */ 
