"use strict";
var mongoose = require("mongoose");
var mongoosePaginate = require("mongoose-paginate");
var uniqueValidator = require('mongoose-unique-validator');
var artistSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: String,
    image: String
});
artistSchema.plugin(mongoosePaginate, uniqueValidator);
var Artist = mongoose.model("Artist", artistSchema);
module.exports = Artist;
