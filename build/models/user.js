"use strict";
var mongoose = require("mongoose");
var uniqueValidator = require('mongoose-unique-validator');
var validator_1 = require("validator");
var userSchema = new mongoose.Schema({
    createdAt: Date,
    name: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    surname: String,
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        validate: [validator_1.isEmail, 'invalid email']
    },
    password: {
        type: String,
        required: true
    },
    image: String,
    role: {
        type: String,
        default: 'ROLE_USER'
    }
});
userSchema.plugin(uniqueValidator);
var User = mongoose.model("User", userSchema);
module.exports = User;
