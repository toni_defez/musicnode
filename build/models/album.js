"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var mongoose_1 = require("mongoose");
var albumSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
    },
    image: String,
    year: Number,
    artist: {
        type: mongoose_1.Schema.Types.ObjectId, ref: 'Artist',
        required: true
    }
});
var Album = mongoose.model("Album", albumSchema);
exports.default = Album;
/*
ALBUM ORIGINAL USANDO JS , CON TYPESCRIPT ESTA ARRIBA
'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AlbumSchema = Schema({
    title:String,
    description:String,
    year:Number,
    artist:{
        type:Schema.ObjectId,
        ref:'Artist'
    }
});

module.exports = mongoose.model('Album',AlbumSchema);

**/ 
