"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = require("./app");
var user_model_1 = require("./routes/user.model");
var welcome_model_1 = require("./routes/welcome.model");
var artist_model_1 = require("./routes/artist.model");
var album_model_1 = require("./routes/album.model");
var song_model_1 = require("./routes/song.model");
var auth_model_1 = require("./routes/auth.model");
var app = new app_1.App([
    new user_model_1.UserRouter(),
    new welcome_model_1.WelcomeRouter(),
    new artist_model_1.ArtistRouter(),
    new album_model_1.AlbumRouter(),
    new song_model_1.SongRouter(),
    new auth_model_1.AuthRouter()
]);
app.listen();
