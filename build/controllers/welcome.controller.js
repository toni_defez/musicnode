"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WelcomeController = /** @class */ (function () {
    function WelcomeController() {
    }
    WelcomeController.prototype.greeting = function (req, res) {
        res.send('Hola, World!');
    };
    ;
    WelcomeController.prototype.greeting2 = function (req, res) {
        var name = req.params.name;
        res.send("Hello, " + name);
    };
    return WelcomeController;
}());
exports.WelcomeController = WelcomeController;
