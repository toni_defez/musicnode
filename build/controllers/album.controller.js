"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var album_1 = __importDefault(require("../models/album"));
var song_1 = __importDefault(require("../models/song"));
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var validator_1 = require("validator");
var AlbumController = /** @class */ (function () {
    function AlbumController() {
    }
    /**
     * Obtenemos todos los albums
     * @param req
     * @param res
     */
    AlbumController.prototype.getAll = function (req, res) {
        var albums = album_1.default.find(function (err, albums) {
            if (err) {
                res.status(500).send({ message: 'Error' });
            }
            else {
                res.status(200).send({ albums: albums });
            }
        });
    };
    ;
    /**
     * Guardamos un nuevo album
     * @param req
     * @param res
     */
    AlbumController.prototype.save = function (req, res) {
        var album = new album_1.default(req.body);
        album.save(function (error, albumStored) {
            if (error) {
                res.status(500).send({ message: 'Error al guardar el album', error: error });
            }
            else {
                if (!albumStored) {
                    res.status(404).send({ message: 'No se ha registrado el album' });
                }
                else {
                    res.status(200).send({
                        "ok": true,
                        "album": albumStored,
                    });
                }
            }
        });
    };
    /**
     * Obtenemos un album por id
     * @param req
     * @param res
     */
    AlbumController.prototype.getAlbumId = function (req, res) {
        var albumId = req.params.id;
        if (validator_1.isMongoId(albumId)) {
            album_1.default.findById(albumId).populate({ path: 'artist' }).exec(function (error, albumStored) {
                if (error) {
                    res.status(500).send({ message: 'Error en la peticion', error: error });
                }
                else {
                    if (!albumStored) {
                        res.status(404).send({ message: 'Album no encontrado' });
                    }
                    else {
                        res.status(200).send({ albumStored: albumStored });
                    }
                }
            });
        }
        else {
            res.status(404).send({ message: 'valor no encontrado' });
        }
    };
    /**
     * Obtenemos todos los albums por artista
     * @param req
     * @param res
     */
    AlbumController.prototype.getAlbumByArtist = function (req, res) {
        var artistId = req.params.artist ? req.params.artist : "";
        var find;
        if (!artistId && artistId == "") {
            find = album_1.default.find({}).sort('title');
        }
        else {
            find = album_1.default.find({ artist: artistId }).sort('year');
        }
        find.populate({ path: 'artist' }).exec(function (error, albums) {
            if (error) {
                res.status(500).send({ message: 'Error en la peticion', error: error });
            }
            else {
                if (!albums) {
                    res.status(404).send({ message: 'Artista sin albums' });
                }
                else {
                    res.status(200).send({ ok: true, albums: albums });
                }
            }
        });
    };
    /**
     * Actualizamos un album
     * @param req
     * @param res
     */
    AlbumController.prototype.updateAlbum = function (req, res) {
        var _this = this;
        var albumId = req.params.id;
        var update = req.body;
        album_1.default.findByIdAndUpdate(albumId, update, function (err, albumUpdate) { return __awaiter(_this, void 0, void 0, function () {
            var currentAlbum;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!err) return [3 /*break*/, 1];
                        res.status(500).send({ message: 'Error al actualizar album' });
                        return [3 /*break*/, 4];
                    case 1:
                        if (!!albumUpdate) return [3 /*break*/, 2];
                        res.status(404).send({ message: 'No se ha podido actualizar al album' });
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, album_1.default.findById(albumId._id)];
                    case 3:
                        currentAlbum = _a.sent();
                        res.status(200).send({ ok: true, album: currentAlbum });
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * Borramos un album por id
     * @param req
     * @param res
     */
    AlbumController.prototype.deleteAlbum = function (req, res) {
        var albumId = req.params.id;
        album_1.default.findByIdAndRemove(albumId, function (error, albumRemoved) {
            if (error) {
                res.status(500).send({ message: 'Error al borrar album', error: error });
            }
            if (!albumRemoved) {
                res.status(404).send({ message: 'No se ha podido borrar al album' });
            }
            else {
                song_1.default.find({ album: albumRemoved._id }).remove(function (error, songRemoved) {
                    if (!songRemoved) {
                        res.status(404).send({ message: 'No se ha podido borrar las canciones' });
                    }
                    else {
                        res.status(200).send({ ok: true, album: albumRemoved });
                    }
                });
            }
        });
    };
    /**
     * Actualizamos la imagen del album
     * @param req
     * @param res
     */
    AlbumController.prototype.uploadImage = function (req, res) {
        var _this = this;
        var albumId = req.params.id + "";
        var file_name = 'No subido....';
        if (req.files) {
            var image = req.files.image;
            var file_split = image.path.split('/');
            file_name = file_split[2];
            if (image.type == 'image/jpeg' || image.type == 'image/png' || image.type == 'image/gif') {
                album_1.default.findByIdAndUpdate(albumId, { image: file_name }, function (err, albumUpdate) { return __awaiter(_this, void 0, void 0, function () {
                    var currentAlbum;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!err) return [3 /*break*/, 1];
                                res.status(500).send({ message: 'Error al actualizar artist', error: err });
                                return [3 /*break*/, 4];
                            case 1:
                                if (!!albumUpdate) return [3 /*break*/, 2];
                                res.status(404).send({ message: 'No se ha podido actualizar al artist' });
                                return [3 /*break*/, 4];
                            case 2: return [4 /*yield*/, album_1.default.findById(albumUpdate._id)];
                            case 3:
                                currentAlbum = _a.sent();
                                res.status(200).send({ ok: true, album: currentAlbum });
                                _a.label = 4;
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
            }
            else {
                res.status(200).send({ message: 'Extension del archivo no valida' });
            }
        }
        else {
            res.status(200).send({ message: 'No has subido ninguna imagen...' });
        }
    };
    /**
     * Obtenemos la imagen de un album
     * @param req
     * @param res
     */
    AlbumController.prototype.getImageFile = function (req, res) {
        var imageFile = req.params.imageFile;
        var path_File = './uploads/album/' + imageFile;
        fs.exists(path_File, function (exists) {
            if (exists) {
                res.status(200).sendFile(path.resolve(path_File));
            }
            else {
                res.status(200).send({ message: "No existe la imagen" });
            }
        });
    };
    return AlbumController;
}());
exports.AlbumController = AlbumController;
