"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var User = require("../models/user");
var jwt2 = __importStar(require("jwt-simple"));
var bcrypt = __importStar(require("bcrypt-nodejs"));
var jwt = __importStar(require("../middlewares/checkJWT"));
var secret = "MusicLiveDaw";
var moment = require("moment");
var AuthController = /** @class */ (function () {
    function AuthController() {
    }
    /**
     * Metodo para logear a un usuario
     * @param req
     * @param res
     */
    AuthController.prototype.loginUser = function (req, res) {
        var params = req.body;
        var email = params.email;
        var password = params.password;
        var user = User.findOne({ "email": email }, function (err, user) {
            if (err) {
                res.status(500).send({ message: "Error en la peticion", error: err });
            }
            else {
                if (!user) {
                    res.status(404).send({ message: "El usuario no existe" });
                }
                else {
                    bcrypt.compare(password, user.password + "", function (error, isMatch) {
                        if (isMatch) {
                            if (params.gethash) {
                                res.status(200).send({
                                    token: jwt.createToken(user)
                                });
                            }
                            else {
                                res.status(200).send({ user: user });
                            }
                        }
                        else {
                            res.status(404).send({ message: "Contraseña incorrecta" });
                        }
                    });
                }
            }
        });
    };
    /**
     * Metodo para registrar usuario
     * @param req
     * @param res
     */
    AuthController.prototype.registerUser = function (req, res) {
        var checkPassword = function doSthWithUser(user) {
            if (user.password) {
                bcrypt.hash(user.password.toString(), '', function () { }, function (err, hash) {
                    user.password = hash;
                });
                return true;
            }
            else {
                return false;
            }
        };
        var user = new User(req.body);
        if (checkPassword(user)) {
            user.save(function (error, userStored) {
                if (error) {
                    res.status(500).send({ message: "Error al registrar usuario", error: error });
                }
                else {
                    if (!userStored) {
                        res.status(404).send({ message: 'No se ha registrado el usuario' });
                    }
                    else {
                        res.status(200).send({
                            "ok": true,
                            "user": userStored
                        });
                    }
                }
            });
        }
        else {
            res.status(200).send({ message: 'Introduzca contraseña' });
        }
    };
    /**
     * Metodo para validar token
     * @param req
     * @param res
     */
    AuthController.prototype.validateToken = function (req, res) {
        var authorization = req.headers.authorization ? req.headers.authorization : "";
        var payload;
        var resultado = {};
        if (!authorization || authorization == "") {
            resultado = {
                error: false
            };
        }
        else {
            var token = authorization.replace(/['"]+/g, '');
            try {
                payload = jwt2.decode(token, secret);
                if (payload.exp <= moment().unix()) {
                    resultado = {
                        error: false
                    };
                }
                else {
                    resultado = {
                        error: true
                    };
                }
            }
            catch (error) {
                resultado = {
                    error: false
                };
            }
        }
        res.send(JSON.stringify(resultado));
    };
    return AuthController;
}());
exports.AuthController = AuthController;
