"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var song_1 = __importDefault(require("../models/song"));
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var SongController = /** @class */ (function () {
    function SongController() {
    }
    /**
     * Obtenemos todas las canciones de la base de datos
     * @param req
     * @param res
     */
    SongController.prototype.getAllSong = function (req, res) {
        var songs = song_1.default.find(function (err, songs) {
            if (err) {
                res.status(500).send({
                    message: 'Error'
                });
            }
            else {
                res.status(200).send({
                    ok: true,
                    songs: songs
                });
            }
        });
    };
    ;
    /**
     * Guardamos la cancion que es pasada por Post
     * @param req
     * @param res
     */
    SongController.prototype.saveSong = function (req, res) {
        var song = new song_1.default(req.body);
        song.save(function (error, songStored) {
            if (error) {
                res.status(500).send({ message: 'Error al guardar la cancion', error: error });
            }
            else {
                if (!songStored) {
                    res.status(404).send({ message: 'No se ha  guardado la cancion' });
                }
                else {
                    res.status(200).send({
                        "ok": true,
                        "song": songStored
                    });
                }
            }
        });
    };
    /**
     * Obtenemos la cancion por ID
     * @param req
     * @param res
     */
    SongController.prototype.getSongId = function (req, res) {
        var songId = req.params.id;
        song_1.default.findById(songId).populate({ path: 'album' }).exec(function (error, songStored) {
            if (error) {
                res.status(500).send({ message: 'Error en la peticion', error: error });
            }
            else {
                if (!songStored) {
                    res.status(404).send({ message: 'Album no encontrado' });
                }
                else {
                    res.status(200).send({ ok: true, song: songStored });
                }
            }
        });
    };
    /**
     * Sacamos todas las canciones por album, basandonos en el parametros Album
     * que nos pasan , en caso de no pasarnos ninguno sacaremos todos los albums
     * @param req
     * @param res
     */
    SongController.prototype.getSongsByAlbum = function (req, res) {
        var albumId = req.params.album;
        var find;
        if (!albumId) {
            find = song_1.default.find({}).sort('number');
        }
        else {
            find = song_1.default.find({ album: albumId }).sort('number');
        }
        find.populate({
            path: 'album',
            populate: {
                path: 'artist',
                model: 'Artist'
            }
        }).exec(function (error, songs) {
            if (error) {
                res.status(500).send({ message: 'Error en la peticion', error: error });
            }
            else {
                if (!songs) {
                    res.status(404).send({ message: 'No hay canciones' });
                }
                else {
                    res.status(200).send({ ok: true, sons: songs });
                }
            }
        });
    };
    /**
     * Obtenemos el fichero de audio asociado a la cancion
     * @param req
     * @param res
     */
    SongController.prototype.updateSong = function (req, res) {
        var _this = this;
        var songId = req.params.id;
        var update = req.body;
        song_1.default.findByIdAndUpdate(songId, update, function (err, songUpdate) { return __awaiter(_this, void 0, void 0, function () {
            var currentSong;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!err) return [3 /*break*/, 1];
                        res.status(500).send({ message: 'Error al actualizar cancion' });
                        return [3 /*break*/, 4];
                    case 1:
                        if (!!songUpdate) return [3 /*break*/, 2];
                        res.status(404).send({ message: 'No se ha podido actualizar la cancion' });
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, song_1.default.findById(songUpdate._id)];
                    case 3:
                        currentSong = _a.sent();
                        res.status(200).send({ ok: true, song: currentSong });
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * Borramos la cancion asignada
     * @param req
     * @param res
     */
    SongController.prototype.deleteSong = function (req, res) {
        var songId = req.params.id;
        song_1.default.findByIdAndRemove(songId, function (error, songRemoved) {
            if (error) {
                res.status(500).send({ message: 'Error en el servidor', error: error });
            }
            if (!songRemoved) {
                res.status(404).send({ message: 'No se ha podido borrar las cancion' });
            }
            else {
                res.status(200).send({ ok: true, song: songRemoved });
            }
        });
    };
    /**
     * Metodo para subir fichero de audio
     * @param req
     * @param res
     */
    SongController.prototype.uploadFile = function (req, res) {
        var songId = req.params.id + "";
        var file_name = 'No subido....';
        console.log(req.files);
        if (req.files) {
            var image = req.files.file;
            var file_split = image.path.split('/');
            file_name = file_split[2];
            if (image.type == 'audio/mpeg') {
                song_1.default.findByIdAndUpdate(songId, { file: file_name }, function (err, songUpdate) {
                    if (err) {
                        res.status(500).send({ message: 'Error al actualizar cancion', error: err });
                    }
                    else {
                        if (!songUpdate) {
                            res.status(404).send({ message: 'No se ha podido actualizar al artist' });
                        }
                        else {
                            var currentSong = song_1.default.findById(songUpdate._id);
                            res.status(200).send({ ok: true, song: currentSong });
                        }
                    }
                });
            }
            else {
                res.status(200).send({ message: 'Extension del archivo no valida' });
            }
        }
        else {
            res.status(200).send({ message: 'No has subido ninguna imagen...' });
        }
    };
    /**
     * Obtenemos el fichero de audio asociado
     * @param req
     * @param res
     */
    SongController.prototype.getSongFile = function (req, res) {
        var imageFile = req.params.songFile;
        var path_File = './uploads/song/' + imageFile;
        fs.exists(path_File, function (exists) {
            if (exists) {
                res.status(200).sendFile(path.resolve(path_File));
            }
            else {
                res.status(200).send({ message: "No existe la imagen" });
            }
        });
    };
    return SongController;
}());
exports.SongController = SongController;
