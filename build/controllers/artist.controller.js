"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var Artist = require("../models/artist");
var song_1 = __importDefault(require("../models/song"));
var album_1 = __importDefault(require("../models/album"));
var ArtistController = /** @class */ (function () {
    function ArtistController() {
    }
    /**
     * Obtengo a todos los artistas
     * @param req
     * @param res
     */
    ArtistController.prototype.getAllArtist = function (req, res) {
        Artist.find(function (err, artists) {
            if (err) {
                res.status(500).send({ message: "Error!" });
            }
            else {
                res.send({
                    ok: true,
                    artists: artists
                });
            }
        });
    };
    ;
    /**
     * Guardo a un nuevo artista
     * @param req
     * @param res
     */
    ArtistController.prototype.saveArtist = function (req, res) {
        var artist = new Artist(req.body);
        artist.save(function (error, artistStored) {
            if (error) {
                res.status(500).send({ message: 'Error al guardar el artista', error: error });
            }
            else {
                if (!artistStored) {
                    res.status(404).send({ message: 'No se ha registrado el artista' });
                }
                else {
                    res.status(200).send({
                        "ok": true,
                        "artist": artistStored
                    });
                }
            }
        });
    };
    /**
     * Obtengo un artista por Id
     * @param req
     * @param res
     */
    ArtistController.prototype.getArtistID = function (req, res) {
        var artistId = req.params.id;
        Artist.findById(artistId, function (err, artist) {
            if (err) {
                res.status(500).send({ message: 'Error en la peticion' });
            }
            else {
                if (!artist) {
                    res.status(404).send({ message: 'El artista no existe' });
                }
                else {
                    res.status(200).send({
                        ok: true,
                        artist: artist
                    });
                }
            }
        });
    };
    /**
     * Obtengo los artistas paginados
     * @param req
     * @param res
     */
    ArtistController.prototype.getArtists = function (req, res) {
        var artistId = req.params.id;
        console.log("Hola", artistId);
        var itemPerPage = 3;
        var options = {};
        options.sort = { 'name': 1 };
        options.page = artistId;
        options.limit = 5;
        Artist.paginate({}, options, function (err, value) {
            if (err) {
                console.log(err);
                return res.status(500).send(err);
            }
            console.log('total: ' + value.total);
            console.log('limit: ' + value.limit);
            console.log('page: ' + value.page);
            console.log('pages: ' + value.pages);
            console.log('offset: ' + value.offset);
            console.log('docs: ');
            console.dir(value.docs);
            return res.status(200).send({
                albums: value.docs,
                page: value.page,
            });
        });
    };
    /**
     * Modifico y devuelvo artista con los ultimos valores
     * @param req
     * @param res
     */
    ArtistController.prototype.updateArtist = function (req, res) {
        var _this = this;
        var artistId = req.params.id;
        var update = req.body;
        Artist.findByIdAndUpdate(artistId, update, function (err, artistUpdate) { return __awaiter(_this, void 0, void 0, function () {
            var currentArtist;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!err) return [3 /*break*/, 1];
                        res.status(500).send({ message: 'Error al actualizar artista' });
                        return [3 /*break*/, 4];
                    case 1:
                        if (!!artistUpdate) return [3 /*break*/, 2];
                        res.status(404).send({ message: 'No se ha podido actualizar al artista' });
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, Artist.findById(artistUpdate._id)];
                    case 3:
                        currentArtist = _a.sent();
                        res.status(200).send({ ok: true, artist: currentArtist });
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * Borrar artista por Id
     * @param req
     * @param res
     */
    ArtistController.prototype.deleteArtist = function (req, res) {
        var artistId = req.params.id;
        Artist.findByIdAndRemove(artistId, function (err, artistRemoved) {
            if (err) {
                res.status(500).send({ message: 'Error al borrar artista' });
            }
            else {
                if (!artistRemoved) {
                    res.status(404).send({ message: 'No se ha podido borrar al artista' });
                }
                else {
                    //Borramos album
                    album_1.default.find({ artist: artistRemoved._id }).remove(function (error, albumRemoved) {
                        if (!albumRemoved || error) {
                            res.status(404).send({ message: 'No se ha podido borrar al album' });
                        }
                        else {
                            song_1.default.find({ artist: artistRemoved._id }).remove(function (error, songRemoved) {
                                if (!songRemoved) {
                                    res.status(404).send({ message: 'No se ha podido borrar las canciones' });
                                }
                                else {
                                    res.status(200).send({ message: 'Todo borrado' });
                                }
                            });
                        }
                    });
                }
            }
        });
    };
    /**
     * Subir imagen del artista
     * @param req
     * @param res
     */
    ArtistController.prototype.uploadImage = function (req, res) {
        var _this = this;
        var artistId = req.params.id;
        var userId = req.params.id + "";
        var file_name = 'No subido....';
        if (req.files) {
            var image = req.files.image;
            var file_split = image.path.split('/');
            file_name = file_split[2];
            if (image.type == 'image/jpeg' || image.type == 'image/png' || image.type == 'image/gif') {
                Artist.findByIdAndUpdate(userId, { image: file_name }, function (err, artistUpdate) { return __awaiter(_this, void 0, void 0, function () {
                    var currentArtist;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                console.log(err);
                                if (!err) return [3 /*break*/, 1];
                                res.status(500).send({ message: 'Error al actualizar artist' });
                                return [3 /*break*/, 4];
                            case 1:
                                if (!!artistUpdate) return [3 /*break*/, 2];
                                res.status(404).send({ message: 'No se ha podido actualizar al artist' });
                                return [3 /*break*/, 4];
                            case 2: return [4 /*yield*/, Artist.findById(artistUpdate._id)];
                            case 3:
                                currentArtist = _a.sent();
                                res.status(200).send({ ok: true, artist: currentArtist });
                                _a.label = 4;
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
            }
            else {
                res.status(200).send({ message: 'Extension del archivo no valida' });
            }
        }
        else {
            res.status(200).send({ message: 'No has subido ninguna imagen...' });
        }
    };
    ArtistController.prototype.getImageFile = function (req, res) {
        var imageFile = req.params.imageFile;
        var path_File = './uploads/artist/' + imageFile;
        fs.exists(path_File, function (exists) {
            if (exists) {
                res.status(200).sendFile(path.resolve(path_File));
            }
            else {
                res.status(200).send({ message: "No existe la imagen" });
            }
        });
    };
    return ArtistController;
}());
exports.ArtistController = ArtistController;
