"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var routeBase_model_1 = require("./routeBase.model");
var auth_controller_1 = require("../controllers/auth.controller");
var AuthRouter = /** @class */ (function (_super) {
    __extends(AuthRouter, _super);
    function AuthRouter() {
        var _this = _super.call(this, '/auth') || this;
        _this.controller = new auth_controller_1.AuthController();
        _this.initializeRoutes();
        return _this;
    }
    AuthRouter.prototype.initializeRoutes = function () {
        this.router.post(this.path + "/register", this.controller.registerUser);
        this.router.post(this.path + "/login", this.controller.loginUser);
        this.router.get(this.path + "/token", this.controller.validateToken);
    };
    return AuthRouter;
}(routeBase_model_1.RouterBase));
exports.AuthRouter = AuthRouter;
