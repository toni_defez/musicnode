"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_controller_1 = require("../controllers/user.controller");
var routeBase_model_1 = require("./routeBase.model");
var authentication = __importStar(require("../middlewares/authenticated"));
var multipart = require('connect-multiparty');
var m_upload = multipart({ uploadDir: './uploads/users' });
var UserRouter = /** @class */ (function (_super) {
    __extends(UserRouter, _super);
    function UserRouter() {
        var _this = _super.call(this, '/user') || this;
        _this.controller = new user_controller_1.UserController();
        _this.initializeRoutes();
        return _this;
    }
    UserRouter.prototype.initializeRoutes = function () {
        this.router.get(this.path + "/getAll", this.controller.getAllUser);
        this.router.put(this.path + "/update-user/:id", authentication.EnsureAuth, this.controller.updateUser);
        this.router.post(this.path + "/upload-image-user/:id", [authentication.EnsureAuth, m_upload], this.controller.uploadImage);
        this.router.get(this.path + "/get-image-user/:imageFile", this.controller.getImageFile);
    };
    return UserRouter;
}(routeBase_model_1.RouterBase));
exports.UserRouter = UserRouter;
