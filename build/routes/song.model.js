"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var routeBase_model_1 = require("./routeBase.model");
var song_controller_1 = require("../controllers/song.controller");
var multipart = require('connect-multiparty');
var m_upload = multipart({ uploadDir: './uploads/song' });
var SongRouter = /** @class */ (function (_super) {
    __extends(SongRouter, _super);
    function SongRouter() {
        var _this = _super.call(this, '/song') || this;
        _this.controller = new song_controller_1.SongController();
        _this.initializeRoutes();
        return _this;
    }
    SongRouter.prototype.initializeRoutes = function () {
        this.router.get(this.path + "/getAll", this.controller.getAllSong);
        this.router.post(this.path + "/save", this.controller.saveSong);
        this.router.get(this.path + "/album/:album?", this.controller.getSongsByAlbum);
        this.router.get(this.path + "/:id", this.controller.getSongId);
        this.router.put(this.path + "/:id", this.controller.updateSong);
        this.router.delete(this.path + "/:id", this.controller.deleteSong);
        this.router.post(this.path + "/upload-file-song/:id", [m_upload], this.controller.uploadFile);
        this.router.get(this.path + "/get-file-song/:songFile", this.controller.getSongFile);
    };
    return SongRouter;
}(routeBase_model_1.RouterBase));
exports.SongRouter = SongRouter;
