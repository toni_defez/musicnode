"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var RouterBase = /** @class */ (function () {
    function RouterBase(path) {
        this.path = path;
        this.router = express_1.Router();
    }
    RouterBase.prototype.initializeRoutes = function () {
    };
    return RouterBase;
}());
exports.RouterBase = RouterBase;
