"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var routeBase_model_1 = require("./routeBase.model");
var welcome_controller_1 = require("../controllers/welcome.controller");
var authentication = __importStar(require("../middlewares/authenticated"));
var WelcomeRouter = /** @class */ (function (_super) {
    __extends(WelcomeRouter, _super);
    function WelcomeRouter() {
        var _this = _super.call(this, '/welcome') || this;
        _this.controller = new welcome_controller_1.WelcomeController();
        _this.initializeRoutes();
        return _this;
    }
    WelcomeRouter.prototype.initializeRoutes = function () {
        this.router.get(this.path + "/saluda", authentication.EnsureAuth, this.controller.greeting);
        // console.log(this.router.stack);
    };
    return WelcomeRouter;
}(routeBase_model_1.RouterBase));
exports.WelcomeRouter = WelcomeRouter;
