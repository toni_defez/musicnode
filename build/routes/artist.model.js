"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var routeBase_model_1 = require("./routeBase.model");
var artist_controller_1 = require("../controllers/artist.controller");
var multipart = require('connect-multiparty');
var m_upload = multipart({ uploadDir: './uploads/artist' });
var ArtistRouter = /** @class */ (function (_super) {
    __extends(ArtistRouter, _super);
    function ArtistRouter() {
        var _this = _super.call(this, '/artist') || this;
        _this.controller = new artist_controller_1.ArtistController();
        _this.initializeRoutes();
        return _this;
    }
    ArtistRouter.prototype.initializeRoutes = function () {
        //AÑADIR MIDLEWARE DE AUTHENTICATION
        this.router.get(this.path + "/getAll", this.controller.getAllArtist);
        this.router.post(this.path + "/save-artist", this.controller.saveArtist);
        this.router.get(this.path + "/detail/:id", this.controller.getArtistID);
        this.router.get(this.path + "/:id", this.controller.getArtists);
        this.router.put(this.path + "/:id", this.controller.updateArtist);
        this.router.delete(this.path + "/:id", this.controller.deleteArtist);
        this.router.post(this.path + "/upload-image-artist/:id", [m_upload], this.controller.uploadImage);
        this.router.get(this.path + "/get-image-artist/:imageFile", this.controller.getImageFile);
    };
    return ArtistRouter;
}(routeBase_model_1.RouterBase));
exports.ArtistRouter = ArtistRouter;
