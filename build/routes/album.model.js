"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var routeBase_model_1 = require("./routeBase.model");
var authentication = __importStar(require("../middlewares/authenticated"));
var album_controller_1 = require("../controllers/album.controller");
var multipart = require('connect-multiparty');
var m_upload = multipart({ uploadDir: './uploads/song' });
var AlbumRouter = /** @class */ (function (_super) {
    __extends(AlbumRouter, _super);
    function AlbumRouter() {
        var _this = _super.call(this, '/album') || this;
        _this.controller = new album_controller_1.AlbumController();
        _this.initializeRoutes();
        return _this;
    }
    AlbumRouter.prototype.initializeRoutes = function () {
        //Sin seguridad por ahora
        this.router.get(this.path + "/getAll", this.controller.getAll);
        this.router.post(this.path + "/save", this.controller.save);
        this.router.get(this.path + "/artist/:artist?", this.controller.getAlbumByArtist);
        this.router.get(this.path + "/:id", this.controller.getAlbumId);
        this.router.put(this.path + "/:id", this.controller.updateAlbum);
        this.router.delete(this.path + "/:id", this.controller.deleteAlbum);
        this.router.post(this.path + "/upload-image-album/:id", [authentication.EnsureAuth, m_upload], this.controller.uploadImage);
        this.router.get(this.path + "/get-image-album/:imageFile", this.controller.getImageFile);
    };
    return AlbumRouter;
}(routeBase_model_1.RouterBase));
exports.AlbumRouter = AlbumRouter;
